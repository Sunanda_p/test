﻿// ***********************************************************************
// Assembly         : FizzBuzz.Repository
// Author           : sunanda
// Created          : 03-01-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="IRule.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace FizzBuzz.Web.Repository.DivisibilityRules.Interfaces
{
    using System;

    /// <summary>
    /// Interface IRule
    /// </summary>
    public interface IRule
    {
        /// <summary>
        /// Gets the fizz buzz text.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>System String.</returns>
        string GetFizzBuzzText(int number);
    }
}