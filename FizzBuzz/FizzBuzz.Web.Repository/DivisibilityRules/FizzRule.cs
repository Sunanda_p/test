﻿// ***********************************************************************
// Assembly         : FizzBuzz.Repository
// Author           : sunanda
// Created          : 03-01-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="FizzRule.cs" company="tcs">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace FizzBuzz.Web.Repository.DivisibilityRules
{
    using FizzBuzz.Web.Repository.DivisibilityRules.Interfaces;

    /// <summary>
    /// Class FizzRule.
    /// </summary>
    /// <seealso cref="FizzBuzz.Web.Repository.DivisibilityRules.Interfaces.IRule" />
    public class FizzRule : IRule
    {
        /// <summary>
        /// Gets the fizz buzz text.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns>System String.</returns>
        public string GetFizzBuzzText(int number)
        {
            if (this.IsDivisible(number))
            {
                return "Fizz";
            }

            return null;
        }

        /// <summary>
        /// Determines whether the specified number is divisible.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <returns><c>true</c> if the specified number is divisible; otherwise, <c>false</c>.</returns>
        private bool IsDivisible(int number)
        {
            return (number % 3) == 0;
        }
    }
}
