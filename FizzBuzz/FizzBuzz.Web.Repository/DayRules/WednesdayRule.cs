﻿// ***********************************************************************
// Assembly         : FizzBuzz.Repository
// Author           : sunanda
// Created          : 03-01-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="WednesdayRule.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace FizzBuzz.Web.Repository.DayRules
{
    using FizzBuzz.Web.Repository.DayRules.Interfaces;
    using System;

    /// <summary>
    /// Class WednesdayRule.
    /// </summary>
    /// <seealso cref="FizzBuzz.Repository.DayRules.Interfaces.IDayRule" />
    public class WednesdayRule : IDayRule
    {
        /// <summary>
        /// Check to validate Day is Wednesday
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <param name="fizzBuzzText">The fizz buzz text.</param>
        /// <returns>System String.</returns>
        public string GetFizzBuzzTextByDay(DateTime dateTime, string fizzBuzzText)
        {
            if (dateTime.DayOfWeek == DayOfWeek.Wednesday)
            {
                return fizzBuzzText.Replace("Fizz", "Wizz").Replace("Buzz", "Wuzz");
            }

            return fizzBuzzText;
        }
    }
}
