﻿// ***********************************************************************
// Assembly         : FizzBuzz.Repository
// Author           : sunanda
// Created          : 03-01-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="IDayRule.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace FizzBuzz.Web.Repository.DayRules.Interfaces
{
    using System;

    /// <summary>
    /// Interface IDayRule
    /// </summary>
    public interface IDayRule
    {
        /// <summary>
        /// Gets the fizz buzz text by day.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <param name="fizzBuzzText">The fizz buzz text.</param>
        /// <returns>System String.</returns>
        string GetFizzBuzzTextByDay(DateTime dateTime, string fizzBuzzText);
    }
}
