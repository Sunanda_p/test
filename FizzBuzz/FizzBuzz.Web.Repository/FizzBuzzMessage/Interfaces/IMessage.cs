﻿// ***********************************************************************
// Assembly         : FizzBuzz.Repository
// Author           : sunanda
// Created          : 03-01-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="IMessage.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace FizzBuzz.Web.Repository.FizzBuzzMessage.Interfaces
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Interface IMessage
    /// </summary>
    public interface IMessage
    {
        /// <summary>
        /// Gets the fizz buzz message.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="dateTime">The date time.</param>
        /// <returns>List of String.</returns>
        List<string> GetFizzBuzzMessage(int number, DateTime dateTime);
    }
}
