﻿// ***********************************************************************
// Assembly         : FizzBuzz.Repository
// Author           : sunanda
// Created          : 03-01-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="MessageRepository.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace FizzBuzz.Web.Repository.FizzBuzzMessage
{
    using FizzBuzz.Web.Repository.DayRules.Interfaces;
    using FizzBuzz.Web.Repository.DivisibilityRules.Interfaces;
    using FizzBuzz.Web.Repository.FizzBuzzMessage.Interfaces;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Class MessageRepository.
    /// </summary>
    /// <seealso cref="FizzBuzz.Repository.FizzBuzzMessage.Interfaces.IMessage" />
    public class MessageRepository : IMessage
    {
        /// <summary>
        /// The divisible rules
        /// </summary>
        private readonly IList<IRule> divisibleRules;

        /// <summary>
        /// The day rule
        /// </summary>
        private readonly IDayRule dayRule;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageRepository"/> class.
        /// </summary>
        /// <param name="rules">The rules.</param>
        /// <param name="dayRule">The day rule.</param>
        public MessageRepository(IList<IRule> rules, IDayRule dayRule)
        {
            this.divisibleRules = rules;
            this.dayRule = dayRule;
        }

        /// <summary>
        /// Gets the fizz buzz message.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="dateTime">The date time.</param>
        /// <returns>List of String.</returns>
        public List<string> GetFizzBuzzMessage(int number, DateTime dateTime)
        {
            var fizzBuzzOutputList = new List<string>();
            for (int currentNumber = 1; currentNumber <= number; currentNumber++)
            {
                string outputText = string.Empty;
                foreach (var rule in this.divisibleRules)
                {
                    outputText = rule.GetFizzBuzzText(currentNumber);

                    if (outputText != null)
                    {
                        break;
                    }
                }

                if (outputText != null)
                {
                    outputText = this.dayRule.GetFizzBuzzTextByDay(dateTime, outputText);
                }
                else
                {
                    outputText = currentNumber.ToString();
                }

                fizzBuzzOutputList.Add(outputText);
            }

            return fizzBuzzOutputList;
        }
    }
}
