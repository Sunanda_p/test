﻿// ***********************************************************************
// Assembly         : FizzBuz.Repository.Tests
// Author           : sunanda
// ***********************************************************************
// <copyright file="WednesdayRuleTest.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace FizzBuzz.Web.Tests
{
    using FizzBuzz.Web.Repository.DayRules;
    using FizzBuzz.Web.Repository.DayRules.Interfaces;
    using NUnit.Framework;
    using System;

    /// <summary>
    /// Class WednesdayRuleTest.
    /// </summary>
    [TestFixture]
    public class WednesdayRuleTest
    {
        /// <summary>
        /// The day rule
        /// </summary>
        private IDayRule wednesdayRule;

        /// <summary>
        /// set up
        /// </summary>
        [TestFixtureSetUp]
        public void SetUp()
        {
            this.wednesdayRule = new WednesdayRule();
        }

        /// <summary>
        /// should_ return_ Buzz_ when_ Day_Is_Not_Wednesday.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <param name="fizzBuzzText">The fizz buzz text.</param>
        [TestCase("02/25/2016", "Buzz")]
        public void ShouldReturn_Buzz_When_Day_Is_Not_Wednesday(DateTime dateTime, string fizzBuzzText)
        {
            var result = this.wednesdayRule.GetFizzBuzzTextByDay(dateTime, fizzBuzzText);
            Assert.AreEqual(result, "Buzz");
        }
    }
}
