﻿// ***********************************************************************
// Assembly         : FizzBuz.Repository.Tests
// Author           : sunanda
// Created          : 03-01-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="MessageRepositoryTest.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace FizzBuzz.Web.Tests
{
    using FizzBuzz.Web.Repository.DayRules.Interfaces;
    using FizzBuzz.Web.Repository.DivisibilityRules.Interfaces;
    using FizzBuzz.Web.Repository.FizzBuzzMessage;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Class MessageRepositoryTest.
    /// </summary>
    [TestFixture]
    public class MessageRepositoryTest
    {
        /// <summary>
        /// The fizz rule
        /// </summary>
        private Mock<IRule> fizzRule;

        /// <summary>
        /// The buzz rule
        /// </summary>
        private Mock<IRule> buzzRule;

        /// <summary>
        /// The day rule
        /// </summary>
        private Mock<IDayRule> dayRule;

        /// <summary>
        /// The message repository
        /// </summary>
        private MessageRepository messageRepository;

        /// <summary>        
        /// Sets up.
        /// </summary>
        [TestFixtureSetUp]
        public void SetUp()
        {
            var rules = new List<IRule>();
            this.fizzRule = new Mock<IRule>();
            this.buzzRule = new Mock<IRule>();
            rules.Add(this.fizzRule.Object);
            rules.Add(this.buzzRule.Object);
            this.dayRule = new Mock<IDayRule>();
            this.messageRepository = new MessageRepository(rules, this.dayRule.Object);
        }

        /// <summary>
        /// Gets the buzz message_ should_ return_ list_ of_ messages.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="dateTime">The date time.</param>
        [TestCase(4, "02/21/2016")]
        public void GetFizzBuzzMessage_When_Valid_Input_Given_And_Day_Is_Not_Wednesday_OutputList_Should_Contain_Valid_Content__And_Count_ShouldBe_Equal(int number, DateTime dateTime)
        {
            this.fizzRule.Setup(x => x.GetFizzBuzzText(It.IsAny<int>())).Returns("Fizz");
            this.dayRule.Setup(x => x.GetFizzBuzzTextByDay(It.IsAny<DateTime>(), "Fizz")).Returns("Fizz");
            var outputList = this.messageRepository.GetFizzBuzzMessage(number, dateTime);
            Assert.AreEqual(outputList[0], "1");
            Assert.AreEqual(outputList[1], "2");
            Assert.AreEqual(outputList[2], "Fizz");
            Assert.AreEqual(outputList[3], "4");
            Assert.AreEqual(outputList.Count, 4);
        }

        /// <summary>
        /// Gets the buzz message_ should_ return_ list_ of_ messages.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="dateTime">The date time.</param>
        [TestCase(5, "02/22/2016")]
        public void GetFizzBuzzMessage_When_Number_Is_5_And_Day_Is_NOt_Wednesday_outputList_Index_4_ShouldBe_Buzz(int number, DateTime dateTime)
        {
            this.buzzRule.Setup(x => x.GetFizzBuzzText(number)).Returns("Buzz");
            this.dayRule.Setup(x => x.GetFizzBuzzTextByDay(It.IsAny<DateTime>(), "Buzz")).Returns("Buzz");
            var outputList = this.messageRepository.GetFizzBuzzMessage(number, dateTime);
            Assert.AreEqual(outputList[4], "Buzz");
            Assert.IsNotNull(outputList);
        }

        /// <summary>
        /// Gets the fizz buzz message_ check_ wizz_ when_ day_ is wednesday_ in_ output list.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="dateTime">The date time.</param>
        [TestCase(3, "02/24/2016")]
        public void GetFizzBuzzMessage_Check_Wizz_When_Day_IsWednesday_In_OutputList(int number, DateTime dateTime)
        {
            this.fizzRule.Setup(x => x.GetFizzBuzzText(3)).Returns("Fizz");
            this.dayRule.Setup(x => x.GetFizzBuzzTextByDay(It.IsAny<DateTime>(), "Fizz")).Returns("Wizz");
            var outputList = this.messageRepository.GetFizzBuzzMessage(number, dateTime);
            Assert.AreEqual(outputList[0], "1");
            Assert.AreEqual(outputList[1], "2");
            Assert.AreEqual(outputList[2], "Wizz");
        }
    }
}
