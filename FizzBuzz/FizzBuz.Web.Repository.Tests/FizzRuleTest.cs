﻿// ***********************************************************************
// Assembly         : FizzBuz.Repository.Tests
// Author           : sunanda
// Created          : 03-01-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="FizzRuleTest.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace FizzBuz.Web.Repository.Tests
{
    using FizzBuzz.Web.Repository.DivisibilityRules;
    using FizzBuzz.Web.Repository.DivisibilityRules.Interfaces;
    using NUnit.Framework;

    /// <summary>
    /// Class FizzRuleTest.
    /// </summary>
    [TestFixture]
    public class FizzRuleTest
    {
        /// <summary>
        /// The divided by three rule
        /// </summary>
        private IRule divideByThree;

        /// <summary>
        /// set up
        /// </summary>        
        [TestFixtureSetUp]
        public void SetUp()
        {
            this.divideByThree = new FizzRule();
        }

        /// <summary>
        /// should_ return_ Fizz_ when_ number_ is_ divisible_ by_ three.
        /// </summary>
        /// <param name="number">The number.</param>
        [TestCase(3)]
        [TestCase(24)]
        [TestCase(27)]
        public void WhenDivisibleByThree_Should_Return_Fizz(int number)
        {
            var result = this.divideByThree.GetFizzBuzzText(number);
            Assert.AreEqual(result, "Fizz");
        }

        /// <summary>
        /// should_ return_ null_ when_ number_ is_ not_ divisible_ by_ three.
        /// </summary>
        /// <param name="number">The number.</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(19)]
        public void When_Not_DivisibleByThree_Should_Return_null(int number)
        {
            var result = this.divideByThree.GetFizzBuzzText(number);
            Assert.IsNull(result);
        }  
    }
}
