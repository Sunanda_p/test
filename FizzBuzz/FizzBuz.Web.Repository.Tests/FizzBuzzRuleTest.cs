﻿// ***********************************************************************
// Assembly         : FizzBuz.Repository.Tests
// Author           : sunanda
// Created          : 03-01-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="FizzBuzzRuleTest.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace FizzBuz.Repository.Tests
{
    using FizzBuzz.Web.Repository.DivisibilityRules;
    using FizzBuzz.Web.Repository.DivisibilityRules.Interfaces;
    using NUnit.Framework;

    /// <summary>
    /// Class FizzBuzzRuleTest.
    /// </summary>
    [TestFixture]
    public class FizzBuzzRuleTest
    {
        /// <summary>
        /// The divided by three rule
        /// </summary>
        private IRule divideByThreeAndFive;

        /// <summary>
        /// set up
        /// </summary>
        [TestFixtureSetUp]
        public void SetUp()
        {
            this.divideByThreeAndFive = new FizzBuzzRule();
        }

        /// <summary>
        /// should_ return_ Fizz_Buzz when_ number_ is_ divisible__ by_ three_and_five.
        /// </summary>
        /// <param name="number">The number.</param>
        [TestCase(30)]
        [TestCase(60)]
        [TestCase(15)]
        public void When_DivisibleByThreeAndFive_Should_Return_FizzBuzz(int number)
        {
            var result = this.divideByThreeAndFive.GetFizzBuzzText(number);
            Assert.AreEqual(result, "FizzBuzz");
        }

        /// <summary>
        /// should_ return_ null_ when_ number_ is_ not_ divisible_ by_ three_and_five.
        /// </summary>
        /// <param name="number">The number.</param>
        [TestCase(1)]
        [TestCase(19)]
        [TestCase(22)]
        public void When_Not_DivisibleByThreeAndFive_Should_Return_Number(int number)
        {
            var result = this.divideByThreeAndFive.GetFizzBuzzText(number);
            Assert.IsNull(result);
        }
    }
}
