﻿// ***********************************************************************
// Assembly         : FizzBuz.Repository.Tests
// Author           : sunanda
// Created          : 03-01-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="BuzzRuleTest.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace FizzBuzz.Web.Tests
{
    using FizzBuzz.Web.Repository;
    using FizzBuzz.Web.Repository.DivisibilityRules;
    using FizzBuzz.Web.Repository.DivisibilityRules.Interfaces;
    using NUnit.Framework;

    /// <summary>
    /// Class  BuzzRuleTest.
    /// </summary>
    [TestFixture]
    public class BuzzRuleTest
    {
        /// <summary>
        /// The divided by Five rule
        /// </summary>
        private IRule divideByFive;

        /// <summary>
        /// initialize the set up
        /// </summary>
        [TestFixtureSetUp]
        public void SetUp()
        {
            this.divideByFive = new BuzzRule();
        }

        /// <summary>
        /// should_ return_ Buzz_ when_ number_ is_divisible_ by_ five.
        /// </summary>
        /// <param name="number">The number.</param>
        [TestCase(5)]
        [TestCase(25)]
        [TestCase(10)]
        public void ShouldReturn_Buzz_When_Number_Is_Divisible_By_Five(int number)
        {
            var result = this.divideByFive.GetFizzBuzzText(number);
            Assert.AreEqual(result, "Buzz");
        }

        /// <summary>
        /// should_ return_ null_ when_ number_ is_ not_ divisible_ by_ five.
        /// </summary>
        /// <param name="number">The number.</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(16)]
        public void Should_Return_Null_When_Number_Not_Divisible_By_Five(int number)
        {
            var result = this.divideByFive.GetFizzBuzzText(number);
            Assert.IsNull(result);
        }
    }
}
