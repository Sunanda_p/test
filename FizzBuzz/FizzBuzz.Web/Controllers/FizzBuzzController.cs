﻿// ***********************************************************************
// Assembly         : FizzBuzz
// Author           : sunanda
// Created          : 03-01-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="FizzBuzzController.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace FizzBuz.Web.Controllers
{
    using FizzBuzz.Models;
    using FizzBuzz.Web.Repository.FizzBuzzMessage.Interfaces;
    using PagedList;
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    /// <summary>
    /// Class FizzBuzzController.
    /// </summary>
    /// <seealso cref="System.Web.Mvc.Controller" />
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// The minimum page
        /// </summary>
        private const int Minimumpage = 1;

        /// <summary>
        /// The maximum page
        /// </summary>
        private const int Maximumpage = 20;

        /// <summary>
        /// The repo
        /// </summary>
        private readonly IMessage repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzController"/> class.
        /// </summary>
        /// <param name="messageRepo">The message repo.</param>
        public FizzBuzzController(IMessage messageRepo)
        {
            this.repo = messageRepo;
        }

        /// <summary>
        /// Indexes the specified page.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <returns>Action Result.</returns>
        [HttpGet]
        public ActionResult Index(int page = 1)
        {
            FizzBuzzModel numberModel = new FizzBuzzModel();

            if (this.Session["OutputMessages"] != null)
            {
                var outputMessages = Session["OutputMessages"] as List<string>;
                numberModel.OutputList = outputMessages.ToPagedList(page, Maximumpage);
            }

            return this.View("Index", numberModel);
        }

        /// <summary>
        /// Indexes the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>Action Result.</returns>
        [HttpPost]
        public ActionResult Index(FizzBuzzModel model)
        {
            if (ModelState.IsValid)
            {
                var output = this.repo.GetFizzBuzzMessage(model.Number, DateTime.Now);
                this.Session["OutputMessages"] = output;
                if (output != null)
                {
                    var numbModel = new FizzBuzzModel { OutputList = output.ToPagedList(Minimumpage, Maximumpage) };
                    return this.View("Index", numbModel);
                }                
            }

            return this.View("Index", model);
        }
    }
}
