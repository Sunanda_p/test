﻿// ***********************************************************************
// Assembly         : FizzBuzz
// Author           : sunanda
// Created          : 02-18-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="FizzBuzzModel.cs" company="Tata Consultancy Service">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace FizzBuzz.Models
{
    using System.ComponentModel.DataAnnotations;
    using PagedList;

    /// <summary>
    /// Class FizzBuzzModel.
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>The number.</value>
        [Required(ErrorMessage = "Please Enter a number between 1 and 1000")]
        [Range(1, 1000, ErrorMessage = "Value must be between 1 and 1000")]
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the output list.
        /// </summary>
        /// <value>The output list.</value>
        public IPagedList<string> OutputList { get; set; }
    }
}