// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IoC.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


using FizzBuzz.Web.Repository.DayRules;
using FizzBuzz.Web.Repository.DayRules.Interfaces;
using FizzBuzz.Web.Repository.DivisibilityRules;
using FizzBuzz.Web.Repository.DivisibilityRules.Interfaces;
using FizzBuzz.Web.Repository.FizzBuzzMessage;
using FizzBuzz.Web.Repository.FizzBuzzMessage.Interfaces;
using StructureMap;
using StructureMap.Graph;
using System.Web.Mvc;
namespace FizzBuzz.Web.DependencyResolution {
    public static class IoC {
        public static IContainer Initialize() {
            ControllerBuilder.Current.SetControllerFactory(new CustomControllerFactory());
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    }); x.For<IRule>().Use<FizzRule>();
                            x.For<IRule>().Use<BuzzRule>();
                            x.For<IDayRule>().Use<WednesdayRule>();
                            x.For<IMessage>().Use<MessageRepository>();
                        });
            return ObjectFactory.Container;
        }
    }
}