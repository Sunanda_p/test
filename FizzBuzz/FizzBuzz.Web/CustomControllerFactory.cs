﻿// ***********************************************************************
// Assembly         : FizzBuzz
// Author           : sunanda
// Created          : 02-18-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="CustomControllerFactory.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace FizzBuzz
{
    using StructureMap;
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// Class CustomControllerFactory.
    /// </summary>
    /// <seealso cref="System.Web.Mvc.DefaultControllerFactory" />
    public class CustomControllerFactory : DefaultControllerFactory
    {
        /// <summary>
        /// Retrieves the controller instance for the specified request context and controller type.
        /// </summary>
        /// <param name="requestContext">The context of the HTTP request, which includes the HTTP context and route data.</param>
        /// <param name="controllerType">The type of the controller.</param>
        /// <returns>The controller instance.</returns>
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return (IController)ObjectFactory.GetInstance(controllerType);
        }
    }
}