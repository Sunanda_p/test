﻿// ***********************************************************************
// Assembly         : FizzBuzz.Tests
// Author           : sunanda
// Created          : 03-01-2016
//
// Last Modified By : sunanda
// Last Modified On : 03-01-2016
// ***********************************************************************
// <copyright file="FizzBuzzControllerTest.cs" company="TCS">
//     Copyright ©  2016
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace FizzBuz.Web.Tests
{
    using FizzBuz.Web.Controllers;
    using FizzBuzz.Models;
    using FizzBuzz.Web.Repository.FizzBuzzMessage.Interfaces;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// Class FizzBuzzControllerTest.
    /// </summary>
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        /// <summary>
        /// The fizz buzz controller
        /// </summary>
        private FizzBuzzController fizzBuzzController;

        /// <summary>
        /// The mock message repo
        /// </summary>
        private Mock<IMessage> mockMessageRepo;

        /// <summary>
        /// The context
        /// </summary>
        private Mock<HttpContextBase> context;

        /// <summary>
        /// The Initialize.
        /// </summary>
        [TestFixtureSetUp]
        public void SetUp()
        {
            this.mockMessageRepo = new Mock<IMessage>();
            this.context = new Mock<HttpContextBase>();
            var session = new Mock<HttpSessionStateBase>();
            this.context.Setup(x => x.Session).Returns(session.Object);
            var requestContext = new RequestContext(this.context.Object, new RouteData());
            this.fizzBuzzController = new FizzBuzzController(this.mockMessageRepo.Object);
            this.fizzBuzzController.ControllerContext = new ControllerContext(requestContext, this.fizzBuzzController);
        }

        /// <summary>
        /// Index HttpGet When Session Is Not Null Case.
        /// Should Set Valid Model
        /// </summary>
        [Test]
        public void WhenSessionIsNotNull_HttpGetIndex_Should_Set_Valid_Model()
        {
            this.context.Setup(x => x.Session["OutputMessages"]).Returns(this.GetFakeSequences());
            ViewResult returnView = this.fizzBuzzController.Index(1) as ViewResult;
            var fizzBuzzModel = (FizzBuzzModel)returnView.Model;
            Assert.IsNotNull(fizzBuzzModel);
        }

        /// <summary>
        /// When output is not null should return model.
        /// </summary>
        [Test]
        public void WhenOutputIsNotNull_In_HttpPost_Index__ShouldSetModel_And_Count_Of_OutputList_Page_One_Should_Match()
        {
            this.mockMessageRepo.Setup(x => x.GetFizzBuzzMessage(22, It.IsAny<DateTime>())).Returns(this.GetFakeSequences());
            ViewResult view = this.fizzBuzzController.Index(new FizzBuzzModel { Number = 22 }) as ViewResult;
            var fizzBuzzModel = (FizzBuzzModel)view.Model;
            Assert.IsNotNull(fizzBuzzModel);
            Assert.AreEqual(fizzBuzzModel.OutputList.Count, 20);
        }

        /// <summary>
        /// The controller get page test.
        /// </summary>
        [Test]
        public void Get_Page_2_From_OutputList()
        {
            this.context.Setup(x => x.Session["OutputMessages"]).Returns(this.GetFakeSequences());
            var result = this.fizzBuzzController.Index(2) as ViewResult;
            var fizzBuzzModel = (FizzBuzzModel)result.Model;
            Assert.AreEqual(fizzBuzzModel.OutputList.Count, 2);
            Assert.IsInstanceOf(typeof(ViewResult), result);
        }

        #region Private Methods
        /// <summary>
        /// The fake sequences.
        /// </summary>
        /// <returns>The <see cref="List" />.</returns>
        private List<string> GetFakeSequences()
        {
            return new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz", "16", "17", "Fizz", "19", "Buzz", "Fizz", "22" };
        }
        #endregion
    }
}
